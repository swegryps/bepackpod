# Disclaimer
bepackpod is a simple bash-script to sync files between two places using rsync.

bepackpod is Free Software and all of the source code is licensed under
the GPL, version 3 or higher (at your option).


# Installation

To install bepackpod copy it to any place defined in PATH. If you are a single user you could put it in ~/bin f.ex.

As named in the disclaimer bepackpod depends on *rsync*. Furthermore *df* is used. 

Before the first use you have to set the variables _kaella (source) and _maal (target) in the head of the script.


# Usage

	bepackpod [SOURCE] [TARGET]

If no argument is given the script uses the defaults you wrote in the head of the script. That's the behaviour bepackpod originally was written for.

A single argument is handled as target and the source is set to the default.

More than 2 arguments are not allowed.

The syncing-process is one-way. That means in the source-map  nothing will be changed, but on the target all files not found in the source will be deleted or changed.


# Name

The name of the script is a playful merge of two words indicating my default use of it: the german verb bepacken (load,pack) and pod as in podcast. I use it daily to sync the podcast-map on my laptop with the corresponding map on my SANSA devices.

# Contact

You can contact me for questions and suggestions:

mail: gryps@fripost.org
XMPP: folky@jabber.se

<!--  LocalWords:  bepackpod df kaella maal bepacken
 -->
